## ppx_rp

Small ppx to extend some `let` monadic to `match` expressions:

- `match?> e with cases` gives:
```ocaml
Lwt.bind e (function cases)
```
or
```ocaml
Lwt.try_bind e (function cases) (function exn_cases)
```
when some exceptions are present in the cases.

- `match?|> e with cases` gives:
```ocaml
Lwt.map (function cases) e
```
