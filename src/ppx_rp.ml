open Ppxlib
open Ast_builder.Default

let is_catchall c =
  let rec aux p = match p.ppat_desc with
    | Ppat_any | Ppat_var _ -> true
    | Ppat_alias (p, _) | Ppat_constraint (p,_) -> aux p
    | _ -> false in
  c.pc_guard = None && aux c.pc_lhs

let mapper =
  object(self)
    inherit Ast_traverse.map as super
    method! expression e =
      let loc = e.pexp_loc in
      match e.pexp_desc with
      | Pexp_match ({
          pexp_desc=Pexp_apply ({
              pexp_desc=Pexp_apply ({
                  pexp_desc=Pexp_ident {txt=Lident s; _}; _},
                  [_ , x]); _}, l); _},
          cases) when s = "?>" || s = "?|>" ->
        let cases =
          if s = "?>" then cases
          else List.map (fun p -> { p with pc_rhs = eapply ~loc (evar ~loc "Lwt.return") [ p.pc_rhs ] }) cases in
        let cases, exns, has_catchall = List.fold_left (fun (c, e, a) p -> match p.pc_lhs.ppat_desc with
            | Ppat_exception pc_lhs ->
              let p = {p with pc_lhs} in
              c, e @ [ p ], a || is_catchall p
            | _ -> c @ [ p ], e, a) ([], [], false) cases in
        let e = match l with [] -> x | _ -> pexp_apply ~loc x l in
        let e = match exns with
          | [] -> eapply ~loc (evar ~loc "Lwt.bind") [ e; pexp_function ~loc cases ]
          | _ ->
            let exns =
              if has_catchall then exns
              else exns @ [
                  case ~lhs:(pvar ~loc "exn") ~guard:None
                    ~rhs:(eapply ~loc (evar ~loc "Lwt.fail") [ evar ~loc "exn" ]) ] in
            eapply ~loc (evar ~loc "Lwt.try_bind") [
              pexp_fun ~loc Nolabel None (punit ~loc) e;
              pexp_function ~loc cases; pexp_function ~loc exns ] in
        (match Sys.getenv_opt "PPX_RP_DEBUG" with
         | Some "true" | Some "1" -> Pprintast.expression Format.err_formatter e
         | _ -> ());
        self#expression e
      | _ -> super#expression e
  end

let () =
  Driver.register_transformation "ppx_rp" ~impl:mapper#structure
